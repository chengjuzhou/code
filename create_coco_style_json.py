import cv2
import os, sys
import json
from xml.dom.minidom import Document

from PIL import Image
from PIL import ImageDraw
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import datetime

# all_classes = ['car', 'person', 'rider', 'truck', 'bus', 'train', 'motorcycle', 'bicycle']
# class_dict = {'car': 1, 'person': 2, 'rider': 3, 'truck': 4, 'bus': 5, 'train': 6, 'motorcycle': 7, 'bicycle': 8}


all_classes = ['person', 'rider', 'car', 'truck', 'bus', 'train', 'motorcycle', 'bicycle']
class_dict = {'person': 1, 'rider': 2, 'car': 3, 'truck': 4, 'bus': 5, 'train': 6, 'motorcycle': 7, 'bicycle': 8}


#CLASSES = ('__background__', 'person', 'rider', 'car', 'truck', 'bus', 'train', 'motorcycle', 'bicycle')

dataset_path = '../../gtFine_trainvaltest/gtFine/'
im_dataset_path = '../../leftImg8bit_trainvaltest/leftImg8bit/'

data_dict = []

info_t = {
	'description': 'CityScapes Dataset',
	'url': 'https://www.cityscapes-dataset.com/',
	'version': '1.0',
	'year': 2021,
	'contributionor': 'Chengju Zhou',
	'data_created': datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'),
}

license_t = [
	{
		'url': 'no list',
		'id': 1,
		'name': 'no name'
	},
	{
		'url': 'no list',
		'id': 2,
		'name': 'no name'
	},
]

categories = []
for i, v in enumerate(class_dict):
	categories.append(
		{
			'supercategory': v,
			'id': class_dict[v],
			'name': v,
		}
	)
	a = 1


set_name = 'val/'
#set_name = 'train/'

images = []
annotations = []

def PolyArea(x,y):
	return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))

def main():

	anno_count = 0

	image_count = 0
	image_id_offset = 0
	if 'val' in set_name:
		image_id_offset = 100000

	main_listing = os.listdir(dataset_path + set_name)
	cnt = 0

	for vid in main_listing:
		tmp_dir = dataset_path + set_name + vid
		im_tmp_dir = im_dataset_path + set_name + vid
		tmp_listing = os.listdir(tmp_dir)

		for tmp_vid in tmp_listing:
			tmp_name = tmp_vid.split('.')[0]
			tmp_name = tmp_name[:-16]
			tmp_form = tmp_vid.split('.')[1]

			if (tmp_form != 'json'):
				continue

			tmp_file = open(tmp_dir + "/" +tmp_vid, 'r')
			tmp_json = json.loads(tmp_file.read())



			#
			im_name = im_tmp_dir + "/" + tmp_vid.split('_gt')[0] + '_leftImg8bit.png'
			im = Image.open(im_name)

			im_path_short = 'leftImg8bit_trainvaltest/leftImg8bit/' + im_name.split('/leftImg8bit/')[-1]

			# fig, ax = plt.subplots(2, 1)
			# [axs.set_axis_off() for axs in ax.ravel()]
			# ax[0].imshow(im)

			im_w = im.width
			im_h = im.height

			# set images list
			image_id = image_count + image_id_offset
			image_count = image_count + 1
			images.append(
				{
					'license': 1,
					'file_name': im_path_short,
					'height': im_h,
					'width': im_w,
					'date_captured': '',
					'id': image_id,
				}
			)

			bbox = []

			im1 = ImageDraw.Draw(im)

			for obj_i in tmp_json['objects']:
				obj_i_label = obj_i['label']
				if not obj_i_label in all_classes:
					continue

				xy = [ (p[0], p[1]) for p in obj_i['polygon']]

				# im1 = ImageDraw.Draw(im)
				im1.polygon(xy, fill=None, outline="blue")
				# im.show()

				xy_np = np.asarray(xy, dtype=np.int)
				x1, y1 = int(min(xy_np[:, 0])), int(min(xy_np[:, 1]))
				x2, y2 = int(max(xy_np[:, 0])), int(max(xy_np[:, 1]))

				bbox_w, bbox_h = int(x2-x1), int(y2-y1)

				if (x1 < 0):
					x1 = 0
				if (x2 >= im_w):
					x2 = im_w - 1
				if (y1 < 0):
					y1 = 0
				if (y2 >= im_h):
					y2 = im_h - 1

				lxyxy = [class_dict[obj_i_label], x1, y1, x2, y2]

				segmentation = xy_np.reshape(-1).tolist()
				area = PolyArea(xy_np[:, 0], xy_np[:, 1])
				bbox = [x1, y1, bbox_w, bbox_h]
				category_id = class_dict[obj_i_label]
				anno_id = anno_count
				anno_count = anno_count + 1

				annotations.append(
					{
						'segmentation': [segmentation,],
						'area': float(area),
						'iscrowd': 0,
						'image_id': image_id,
						'bbox': bbox,
						'category_id': category_id,
						'id': anno_id,
					}
				)


				# im1.rectangle([x1, y1, x2, y2], fill=None, outline='blue', width=2)
				# im1.text((x1, y1), obj_i_label, fill='green')

				# rect = patches.Rectangle((x1, y1), x2-x1, y2-y1, linewidth=1, edgecolor='g', facecolor='none')
				# ax[0].add_patch(rect)
				# ax[0].text(x1, y1, obj_i_label, size=12, color='g')

				#bbox.append(lxyxy)

				a = 1

			#bbox = np.asarray(bbox)
			a = 1

			#im.show()

			# data_dict.append({
			# 	'width': im_w,
			# 	'height': im_h,
			# 	'label_xyxy': bbox.tolist(),
			# 	'im_path': im_path_short,
			# }
			# )

			# doc = Document()
			# tmp_anno = doc.createElement('annotation')
			# doc.appendChild(tmp_anno)
			#
			# tmp_folder = doc.createElement('folder')
			# folder_text = doc.createTextNode('Cityscapes')
			# tmp_folder.appendChild(folder_text)
			# tmp_anno.appendChild(tmp_folder)
			#
			# tmp_filename = doc.createElement('filename')
			# file_name = doc.createTextNode(tmp_name + '.png')
			# tmp_filename.appendChild(file_name)
			# tmp_anno.appendChild(tmp_filename)
			#
			# height = tmp_json['imgHeight']
			# width = tmp_json['imgWidth']
			#
			# tmp_size = doc.createElement('size')
			# tmp_depth = doc.createElement('depth')
			# depth_text = doc.createTextNode('3')
			# tmp_depth.appendChild(depth_text)
			# tmp_size.appendChild(tmp_depth)
			# tmp_width = doc.createElement('width')
			# width_text = doc.createTextNode(str(width))
			# tmp_width.appendChild(width_text)
			# tmp_size.appendChild(tmp_width)
			# tmp_height = doc.createElement('height')
			# height_text = doc.createTextNode(str(height))
			# tmp_height.appendChild(height_text)
			# tmp_size.appendChild(tmp_height)
			# tmp_anno.appendChild(tmp_size)
			#
			# tmp_obj = tmp_json['objects']
			# for inst in tmp_obj:
			# 	tmp_label = inst['label']
			#
			# 	if not (tmp_label in all_classes):
			# 		continue
			#
			# 	class_dict[tmp_label] = class_dict[tmp_label] + 1
			#
			# 	tmp_poly = inst['polygon']
			# 	tmp_x = []
			# 	tmp_y = []
			#
			# 	for point in tmp_poly:
			# 		tmp_x.append(point[0])
			# 		tmp_y.append(point[1])
			#
			# 	x1 = min(tmp_x)
			# 	x2 = max(tmp_x)
			# 	y1 = min(tmp_y)
			# 	y2 = max(tmp_y)
			#
			# 	if (x1 <= 0):
			# 		x1 = 1
			# 	if (x2 >= width):
			# 		x2 = width - 1
			# 	if (y1 <= 0):
			# 		y1 = 1
			# 	if (y2 >= height):
			# 		y2 = height - 1
			#
			# 	tmp_inst = doc.createElement('object')
			# 	tmp_inst_name = doc.createElement('name')
			# 	inst_name_text = doc.createTextNode(tmp_label)
			# 	tmp_inst_name.appendChild(inst_name_text)
			# 	tmp_inst.appendChild(tmp_inst_name)
			#
			# 	tmp_bndbox = doc.createElement('bndbox')
			# 	tmp_x1 = doc.createElement('xmin')
			# 	x1_text = doc.createTextNode(str(x1))
			# 	tmp_x1.appendChild(x1_text)
			# 	tmp_bndbox.appendChild(tmp_x1)
			# 	tmp_y1 = doc.createElement('ymin')
			# 	y1_text = doc.createTextNode(str(y1))
			# 	tmp_y1.appendChild(y1_text)
			# 	tmp_bndbox.appendChild(tmp_y1)
			# 	tmp_x2 = doc.createElement('xmax')
			# 	x2_text = doc.createTextNode(str(x2))
			# 	tmp_x2.appendChild(x2_text)
			# 	tmp_bndbox.appendChild(tmp_x2)
			# 	tmp_y2 = doc.createElement('ymax')
			# 	y2_text = doc.createTextNode(str(y2))
			# 	tmp_y2.appendChild(y2_text)
			# 	tmp_bndbox.appendChild(tmp_y2)
			# 	tmp_inst.appendChild(tmp_bndbox)
			#
			# 	tmp_anno.appendChild(tmp_inst)
			#
			# tmp_xml = open('anno_test_2/' + tmp_name + '.xml', 'w')
			# # tmp_xml.write(doc.toprettyxml(indent='\t', encoding='utf-8'))

	data_dict = {
		'info': info_t,
		'images': images,
		'licenses': license_t,
		'annotations': annotations,
		'categories': categories,
	}

	with open('cityscapes_' + set_name.replace('/', '')+'_coco_style.json', 'w') as f:
		json.dump(data_dict, f)

	# print(class_dict)


main()