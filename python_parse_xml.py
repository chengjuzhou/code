import xml.etree.ElementTree as ET 

 

 

ann_path = os.path.join(self.annotation_dir, img_id + '.xml') 

        target = self.parse_voc_xml(ET.parse(ann_path).getroot())['annotation'] 

 

 def parse_voc_xml(self, node): 

        voc_dict = {} 

        children = list(node) 

        if children: 

            def_dic = defaultdict(list) 

            for dc in map(self.parse_voc_xml, children): 

                for ind, v in dc.items(): 

                    def_dic[ind].append(v) 

            voc_dict = { 

                node.tag: {k: v if k == 'object' else v[0] for k, v in def_dic.items()} 

            } 

        elif node.text: 

            text = node.text.strip() 

            voc_dict[node.tag] = text 

        return voc_dict 