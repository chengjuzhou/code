from PIL import Image

from PIL import ImageDraw

import matplotlib.pyplot as plt

import matplotlib.patches as patches

import numpy as np

def show_and_check_image(result):
    im = result['img']
    im = Image.fromarray(im)
    bbox = result['boxes']
    label = result['labels']
    label_txt = result['labels_txt']

    im_draw = ImageDraw.Draw(im)

    for i in range(bbox.shape[0]):
        x1, y1, x2, y2 = bbox[i, :]
        label_i = label[i]
        label_i_txt = label_txt[i]
        im_draw.rectangle([x1, y1, x2, y2], fill=None, outline='green', width=2)
        im_draw.text([x1, y1], str(label_i_txt), fill='green')
        a = 1

    im.show()
    a = 1
===========================================================



import torch
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from torchvision import transforms
from torch.nn import functional as F

T2PIL = transforms.ToPILImage()
PIL2T = transforms.ToTensor()

def tensor2image(images):
    im_mean = torch.zeros(images.tensors.shape).squeeze().to(images.tensors.device)
    im_std = torch.zeros(images.tensors.shape).squeeze().to(images.tensors.device)
    im_mean[0, :, :] = 0.485
    im_mean[1, :, :] = 0.456
    im_mean[2, :, :] = 0.406
    im_std[0, :, :] = 0.229
    im_std[1, :, :] = 0.224
    im_std[2, :, :] = 0.225
    im_tensor = images.tensors.clone() * im_std + im_mean
    im = T2PIL(im_tensor.cpu().squeeze())

    return im


def check_images_targets(images, targets, segments=None, seg_label=None, objectness=None, ol=None, ol_label=None, s2=None,
                         scaleseg=None, labels_t_scale=None, labels_t_scale2=None):
    img = tensor2image(images)
    boxes = targets[0].convert('xywh').bbox.cpu()
    fig, ax = plt.subplots(3, 2)
    [axs.set_axis_off() for axs in ax.ravel()]
    ax[0, 0].imshow(img)
    for idx in range(boxes.shape[0]):
        bb = boxes[idx, :]
        print(bb)
        rect = patches.Rectangle((bb[0], bb[1]), bb[2], bb[3], linewidth=1, edgecolor='g', facecolor='none')
        ax[0, 0].add_patch(rect)

    im_w, im_h = targets[0].size
    if segments is not None:
        segments_0  = F.interpolate(segments[0], size=[im_h, im_w], mode="bilinear")
        ax[0, 1].matshow(segments_0.squeeze().detach().cpu().sigmoid())
    if s2 is not None:
        s2_0  = F.interpolate(s2[0], size=[im_h, im_w], mode="bilinear")
        ax[2, 0].matshow(s2_0.squeeze().detach().cpu().sigmoid())

    if objectness is not None:
        objectness_0 = F.interpolate(objectness[0], size=[im_h, im_w], mode='bilinear')
        ax[1, 1].matshow(objectness_0.mean(1).squeeze().detach().cpu().sigmoid())

    for idx in range(boxes.shape[0]):
        bb = boxes[idx, :]
        print(bb)
        rect2 = patches.Rectangle((bb[0], bb[1]), bb[2], bb[3], linewidth=1, edgecolor='g', facecolor='none')
        ax[1, 1].add_patch(rect2)
        rect3 = patches.Rectangle((bb[0], bb[1]), bb[2], bb[3], linewidth=1, edgecolor='g', facecolor='none')
        ax[0, 1].add_patch(rect3)
    #
    # if seg_label is not None:
    #     ax[1, 0].matshow(seg_label.squeeze().detach().cpu())
    #
    # if ol is not None:
    #     ax[2, 1].matshow(ol[0].squeeze().detach().cpu().sigmoid())
    #
    # if ol_label is not None:
    #     ax[2, 0].matshow(ol_label.squeeze().detach().cpu())

    fig2, ax2 = plt.subplots(4, 3)
    [axs.set_axis_off() for axs in ax2.ravel()]
    # ax2[0, 0].imshow(img)
    for idx in range(scaleseg[0].shape[1]):
        x = int(idx/3)
        y = idx - x*3
        ax2[x, y].matshow(scaleseg[0][0, idx, :, :].squeeze().detach().cpu().sigmoid())
        ax2[x, y].title.set_text(str(idx))

    fig3, ax3 = plt.subplots(4, 3)
    [axs.set_axis_off() for axs in ax3.ravel()]
    # ax2[0, 0].imshow(img)
    for idx in range(labels_t_scale[0].shape[1]):
        x = int(idx / 3)
        y = idx - x * 3
        ax3[x, y].matshow(labels_t_scale[0][0, idx, :, :].squeeze().detach().cpu())
        ax3[x, y].title.set_text(str(idx))

    fig4, ax4 = plt.subplots(4, 3)
    [axs.set_axis_off() for axs in ax4.ravel()]
    # ax2[0, 0].imshow(img)
    for idx in range(labels_t_scale[0].shape[1]):
        x = int(idx / 3)
        y = idx - x * 3
        ax4[x, y].matshow(labels_t_scale2[0][0, idx, :, :].squeeze().detach().cpu())
        ax4[x, y].title.set_text(str(idx))


    plt.show()
    plt.close('all')